const LightManager = require('../classes/light-manager').LightManager;
const lightManager = new LightManager();
const path = require('path');

//room light-nr on/off
exports.set = function (req, res) {
    console.log("settings", req.body);
    let settings = req.body;
    let command = lightManager.recieveCommand(settings);
    res.status(200).send(`${command} - ok`);
};
