var router = require('express').Router();
var lightController = require('../controllers/light-controller');

router.post('/set', lightController.set);

exports.lightRouter = router;