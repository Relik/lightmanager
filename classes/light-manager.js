const Logger = require('./logger').Logger;
const SerialPort = require('./serial-port.js').SPort;

const logger = new Logger();
const serialPort = new SerialPort();
// sendMessage('03 D3 57 02 01 FF FF FF FF FF');

class LightManager {

    constructor() {
    }
    recieveCommand(command) {
        let comm = `03 D3 ${command.device} 0${command.relay} 0${command.state} FF FF FF FF FF`;
        console.log('stworzona komenda',comm);
       serialPort.send(comm); //odkomentowac do raspery
       return comm;
    } 
}
exports.LightManager = LightManager;