const raspi = require('raspi');
const gpio = require('raspi-gpio');
var pin;
raspi.init(() => {
    pin = new gpio.DigitalOutput({
    pin: 'GPIO4',
    pullResistor: gpio.PULL_NONE
  });
});

function setMode(mode){
    if(!pin) return;
    mode = mode ? gpio.HIGH : gpio.HIGH;
    return new Promise(function (resolve, reject) {
        pin.write(mode);
        console.log(`Changed state to ${mode}`);
        while(pin.value !== mode){
            //Error handler
        }
        //setTimeout(resolve(), 100);

       resolve();
    })
    
}
exports.setMode = setMode;