var app = require('./routers/index').app;
var server = require('http').createServer(app);

const PORT = process.env.PORT ? process.env.PORT : 3002;

server.listen(PORT, () => {
    console.log(`Hello, I\'m listening on PORT:${PORT}`);
});
