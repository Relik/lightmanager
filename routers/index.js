const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

const rootRouter = require('./root-router').rootRouter;
const lightRouter = require('./light-router').lightRouter;

ROOT_DIRECTORY = path.join(path.dirname(require.main.filename));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.use('/', rootRouter);
app.use('/light', lightRouter);

exports.app = app;
