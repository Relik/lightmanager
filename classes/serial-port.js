const SerialPort = require('serialport')
const Delimiter = require('@serialport/parser-delimiter')
const port = new SerialPort('/dev/ttyAMA0', { baudRate: 19200 })
const START_MSG = "7D7E";
const END_MSG = "7D7F";
const parser = new Delimiter({ delimiter: Buffer.from(START_MSG, 'hex') })
const crc = require('node-crc');
const Logger = require('./logger').Logger;
const logger = new Logger();
const setMode = require('./pin-manager').setMode;
port.pipe(parser)
// 03 f3 57 02 ff ff ff ff ff ff ask for status
// 03 D3 57 02 00 FF FF FF FF FF off as control 
//7D7E 20A0020100FFFFFFFFFF D5E47D7F off as button (2)
//7D7E 20 A0 02 01 01FFFFFFFFFF90447D7F on as button


parser.on('data', line => console.log("Got: ", line))// do otrzymywania info
// sendMessage('03 D3 57 02 01 FF FF FF FF FF');

// sendMessage(' ');
class SPort {
    constructor() {
        setMode(0);
    }
    send(message) {
        let msg = this.createMessage(message);
        setMode(1)
        .then(() => {
                console.log("Sending: ", msg);
                port.write(msg)
                setTimeout(()=>{ setMode(0);}, 500);
               
            })
    }
    calculateCRC16(value) {
        return crc.crc(16, false, 0x1021, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x31C3, value).toString('hex');
        // Arguments: the length of bits, reflection, low bits of expression, high bits of expression, low bits of the initial value, high bits of the initial value, low bits of the final xor value, high bits of the final xor value, the source data buffer
    };
    createMessage(msg) {
        msg = msg.replace(/\s/g, "");
        return Buffer.from(START_MSG + msg + this.calculateCRC16(Buffer.from(msg, 'hex')) + END_MSG, 'hex');
    }
}

exports.SPort = SPort;

